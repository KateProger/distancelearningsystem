﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyDistanceLearningSystem.Models;
using System.IO;
using MyDistanceLearningSystem.Functions;

namespace MyDistanceLearningSystem.Controllers
{
    [Authorize]
    public class ApplicationFilesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: ApplicationFiles
        /*public async Task<ActionResult> Index()
        {
            var applicationFiles = db.ApplicationFiles.Include(a => a.Discipline).Include(a => a.Teacher);
            return View(await applicationFiles.ToListAsync());
        }*/
        public async Task<ActionResult> Index(string Name, int? DisciplineId, string TeacherId, int? ItemCountPerPage, int page = 1)
        {
            int pageSize = ItemCountPerPage != null ? (int)ItemCountPerPage : 5;
            int filesCount = await db.ApplicationFiles.Where(f=>!f.IsHomeWork).CountAsync();
            IEnumerable<ApplicationFile> filesPerPages = await db.ApplicationFiles.Where(f => !f.IsHomeWork).Include(f => f.Discipline).Include(f => f.Teacher).ToListAsync();
            if (!String.IsNullOrEmpty(Name))
            {
                filesPerPages = filesPerPages.Where(d => d.DisplayName.IndexOf(Name) != -1);
                filesCount = filesPerPages.Count();
            }
            if (DisciplineId != null && DisciplineId != 0)
            {
                filesPerPages = filesPerPages.Where(f => f.DisciplineId == DisciplineId);
                filesCount = filesPerPages.Count();
            }
            filesPerPages =
                filesPerPages.OrderBy(f => f.Id).ThenBy(f => f.DisplayName)
                .Skip((page - 1) * pageSize)
                .Take(pageSize).ToList();
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = filesCount };

            var Disciplines = await db.Disciplines.Select(d => new { Id = d.Id, Name = d.Name }).ToListAsync();
            Disciplines.Add(new { Id = 0, Name = "Все дисциплины" });
            ViewBag.Disciplines = new SelectList(Disciplines.OrderBy(d => d.Id), "Id", "Name");

            var Teachers = await db.Users.Select(u => new { Id = u.Id, Name = u.Name }).ToListAsync();
            Teachers.Add(new { Id = Guid.Empty.ToString(), Name = "Все преподаватели" });
            ViewBag.Teachers = new SelectList(Teachers.OrderBy(u => u.Id), "Id", "Name");

            ViewBag.Discipline = (await db.Users.Where(u => String.Equals(u.Email, User.Identity.Name)).FirstOrDefaultAsync()).DisciplineId;
            ViewBag.Teacher = (await db.Users.Where(u => String.Equals(u.Email, User.Identity.Name)).FirstOrDefaultAsync()).Id;
            IndexViewModel<ApplicationFile> ivm = new IndexViewModel<ApplicationFile>
            {
                PageInfo = pageInfo,
                Objects = filesPerPages,
                Name = Name ?? "",
                SelectedDiscipline = DisciplineId ?? 0,
                SelectedTeacher = TeacherId ?? Guid.Empty.ToString()
            };
            return View(ivm);

        }
        
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationFile applicationFile = await db.ApplicationFiles.Include(f => f.Discipline).Include(f => f.Teacher).Where(f=>f.Id == id).FirstOrDefaultAsync();
            if (applicationFile == null)
            {
                return HttpNotFound();
            }
            //.FindAsync(id)
            return PartialView(applicationFile);
        }

        public async Task<ActionResult> GetFile(int? id, bool isHomeWork = false)
        {
            if(id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            ApplicationFile file = await db.ApplicationFiles.FindAsync(id);
            string dir = isHomeWork ? "HomeWorks/" : "Disciplines/";
            string file_path = Server.MapPath("~/Files/" + dir + file.DisciplineId + "/" + file.FileName);
            string file_type = "application/octet-stream";
            string file_name = file.DisplayName + (file.FileName.Split('.').Any() ? "." + file.FileName.Split('.').LastOrDefault() : String.Empty);
            return File(file_path, file_type, file_name);
        }
        
        public async Task<ActionResult> Create()
        {
            string TeacherId = (await db.Users.Where(u => String.Equals(u.Email, User.Identity.Name)).FirstOrDefaultAsync()).Id;
            if (User.IsInRole("admin"))
            {
                ViewBag.DisciplineId = new SelectList(db.Disciplines, "Id", "Name");
            }
            else
            {
                ViewBag.DisciplineId = new SelectList(db.Disciplines.Where(d => db.Users.Where(u => String.Equals(u.Id, TeacherId)).FirstOrDefault().DisciplineId == d.Id), "Id", "Name");
            }
            ViewBag.TeacherId = TeacherId;
            string referrer = HttpContext.Request.UrlReferrer == null ? "/ApplicationFiles" : HttpContext.Request.UrlReferrer.PathAndQuery;
            ViewBag.Referrer = referrer;
            return View();
        }
        
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Id,DisplayName,FileName,TeacherId,UploadDate,DisciplineId")] ApplicationFile applicationFile, HttpPostedFileBase file, string referrer = "")
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    FileInfo fileInf = new FileInfo(Server.MapPath("~/Files/Disciplines/" + applicationFile.DisciplineId + "/" + applicationFile.FileName));
                    /*if (fileInf.Exists)
                    {
                        fileInf.Delete();
                    }*/
                    file.SaveAs(Server.MapPath("~/Files/Disciplines/" + applicationFile.DisciplineId + "/" + Path.GetFileName(file.FileName)));

                    //?
                    applicationFile.FileName = HashCreator.GetHashString(DateTime.Now.ToString() + Path.GetFileName(file.FileName)) + Path.GetExtension(file.FileName);// (DateTime.Now.ToString() + Path.GetFileName(file.FileName)).GetHashCode().ToString();

                    db.ApplicationFiles.Add(applicationFile);
                    await db.SaveChangesAsync();
                    //return RedirectToAction("Index");
                    return Redirect(referrer);

                }
                
            }

            //ViewBag.DisciplineId = new SelectList(db.Disciplines, "Id", "Name", applicationFile.DisciplineId);
            string TeacherId = (await db.Users.Where(u => String.Equals(u.Email, User.Identity.Name)).FirstOrDefaultAsync()).Id;
            if (User.IsInRole("admin"))
            {
                ViewBag.DisciplineId = new SelectList(db.Disciplines, "Id", "Name", applicationFile.DisciplineId);
            }
            else
            {
                ViewBag.DisciplineId = new SelectList(db.Disciplines.Where(d => db.Users.Where(u => String.Equals(u.Id, TeacherId)).FirstOrDefault().DisciplineId == d.Id), "Id", "Name");
            }
            ViewBag.TeacherId = TeacherId; //new SelectList(db.Users, "Id", "Name", applicationFile.TeacherId);
            ViewBag.Referrer = referrer;
            return View(applicationFile);
        }
        
        [HttpGet]
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationFile applicationFile = await db.ApplicationFiles.FindAsync(id);
            if (applicationFile == null)
            {
                return HttpNotFound();
            }
            //ViewBag.DisciplineId = new SelectList(db.Disciplines, "Id", "Name", applicationFile.DisciplineId);
            // string TeacherId = db.Users.Where(u => String.Equals(u.Email, User.Identity.Name)).FirstOrDefault().Id;
            ViewBag.Teacher = (await db.Users.Where(u => String.Equals(applicationFile.TeacherId, u.Id)).FirstAsync()).Name; ;
            if (User.IsInRole("admin"))
            {
                ViewBag.DisciplineId = new SelectList(db.Disciplines, "Id", "Name", applicationFile.DisciplineId);
            }
            else
            {
                
                ViewBag.DisciplineId = new SelectList(db.Disciplines.Where(d => db.Users.Where(u => String.Equals(u.Id, applicationFile.TeacherId)).FirstOrDefault().DisciplineId == d.Id), "Id", "Name");
            }
            string referrer = HttpContext.Request.UrlReferrer == null ? "/ApplicationFiles" : HttpContext.Request.UrlReferrer.PathAndQuery;
            ViewBag.Referrer = referrer;
            return View(applicationFile);
        }
        
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,DisplayName,FileName,TeacherId,UploadDate,DisciplineId")]ApplicationFile applicationFile, HttpPostedFileBase file, string referrer = "")
        {
            if (ModelState.IsValid)
            {
                if (file != null)
                {
                    /*if (fileInf.Exists)
                    {
                        fileInf.Delete();
                    }*/
                    FileInfo fileInf = new FileInfo(Server.MapPath("~/Files/Disciplines/" + applicationFile.DisciplineId + "/" + applicationFile.FileName));
                    if (fileInf.Exists)
                        fileInf.Delete();

                    applicationFile.FileName = HashCreator.GetHashString(DateTime.Now.ToString() + Path.GetFileName(file.FileName)) + Path.GetExtension(file.FileName);//Path.GetFileName(file.FileName);

                    file.SaveAs(Server.MapPath("~/Files/Disciplines/" + applicationFile.DisciplineId + "/" + applicationFile.FileName));

                    

                    db.Entry(applicationFile).State = EntityState.Modified;
                    await db.SaveChangesAsync();
                    //return RedirectToAction("Index");
                    return Redirect(referrer);
                }
                
            }
            //ViewBag.DisciplineId = new SelectList(db.Disciplines, "Id", "Name", applicationFile.DisciplineId);
            string TeacherId = (await db.Users.Where(u => String.Equals(u.Email, User.Identity.Name)).FirstOrDefaultAsync()).Id;
            if (User.IsInRole("admin"))
            {
                ViewBag.DisciplineId = new SelectList(db.Disciplines, "Id", "Name", applicationFile.DisciplineId);
            }
            else
            {
                ViewBag.DisciplineId = new SelectList(db.Disciplines.Where(d => db.Users.Where(u => String.Equals(u.Id, TeacherId)).FirstOrDefault().DisciplineId == d.Id), "Id", "Name");
            }
            //ViewBag.TeacherId = new SelectList(db.Users, "Id", "Name", applicationFile.TeacherId);
            ViewBag.Referrer = referrer;
            return View(applicationFile);
        }

        [HttpGet]
        public async Task<ActionResult> AddFile(int? id, bool isHomeWork = false)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discipline discipline = await db.Disciplines.FindAsync(id);
            if (discipline == null)
            {
                return HttpNotFound();
            }
            ViewBag.DisciplineId = id;
            ViewBag.IsHomeWork = isHomeWork;
            return PartialView(discipline);
        }

        [HttpPost]
        public async Task AddFile(int? disciplineId, string displayName, string comment, HttpPostedFileBase file, bool isHomeWork = false)
        {
            //напиши обработку что не нулл
            //if (disciplineId == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            if (file != null && disciplineId != null)
            {
                string fileName = HashCreator.GetHashString(DateTime.Now.ToString() + Path.GetFileName(file.FileName)) + Path.GetExtension(file.FileName);//Path.GetFileName(file.FileName);
                                                                                                                                                          //  if (!db.ApplicationFiles.Any(f => String.Equals(f.DisplayName, displayName)) && !db.ApplicationFiles.Any(f => String.Equals(f.FileName, fileName)))
                                                                                                                                                          //  {
                string TeacherId = (await db.Users.Where(u => String.Equals(u.Email, User.Identity.Name)).FirstOrDefaultAsync()).Id;
                ApplicationFile newfile = new ApplicationFile() { DisplayName = displayName, FileName = fileName, UploadDate = DateTime.Now, DisciplineId = (int)disciplineId, TeacherId = TeacherId, IsHomeWork = isHomeWork };
                db.ApplicationFiles.Add(newfile);
                await db.SaveChangesAsync();

                string dir = isHomeWork ? "HomeWorks/" : "Disciplines/";
                DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("~/Files/" + dir + disciplineId));
                if (!dirInfo.Exists)
                {
                    dirInfo.Create();
                }
                file.SaveAs(Server.MapPath("~/Files/" + dir + disciplineId + "/" + fileName));

                //}
            }
        }
        
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationFile applicationFile = await db.ApplicationFiles.FindAsync(id);
            if (applicationFile == null)
            {
                return HttpNotFound();
            }
            string referrer = HttpContext.Request.UrlReferrer == null ? "/Disciplines" : HttpContext.Request.UrlReferrer.PathAndQuery;
            ViewBag.Referrer = referrer;
            return View(applicationFile);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id, string referrer = "")
        {
            ApplicationFile applicationFile = await db.ApplicationFiles.FindAsync(id);
            db.ApplicationFiles.Remove(applicationFile);
            await db.SaveChangesAsync();
            
            FileInfo fileInf = new FileInfo(Server.MapPath("~/Files/Disciplines/" + applicationFile.DisciplineId + "/" + applicationFile.FileName));
            if (fileInf.Exists)
                fileInf.Delete();

            //return RedirectToAction("Index");
            return Redirect(referrer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
