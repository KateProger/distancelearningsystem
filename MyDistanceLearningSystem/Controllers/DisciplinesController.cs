﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyDistanceLearningSystem.Models;
using System.IO;
using MyDistanceLearningSystem.Functions;

namespace MyDistanceLearningSystem.Controllers
{
    [Authorize]
    public class DisciplinesController : Controller
    {
        
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Disciplines
        /*public async Task<ActionResult> Index()
        {
            var disciplines = db.Disciplines.Include(d => d.TrainingProgram);
            return View(await disciplines.ToListAsync());
        }*/
        public async Task<ActionResult> Index(string Name, int? TrainingProgramId, int? ItemCountPerPage, int page = 1)
        {
            int pageSize = ItemCountPerPage != null ? (int)ItemCountPerPage : 5;
            int disciplinesCount = await db.Disciplines.CountAsync();
            IEnumerable<Discipline> disciplinesPerPages = await db.Disciplines.Include(d => d.TrainingProgram).ToListAsync();
            if (!String.IsNullOrEmpty(Name))
            {
                disciplinesPerPages = disciplinesPerPages.Where(d => d.Name.IndexOf(Name) != -1);
                disciplinesCount = disciplinesPerPages.Count();
            }
            if (TrainingProgramId != null && TrainingProgramId != 0)
            {
                disciplinesPerPages = disciplinesPerPages.Where(d => d.TrainingProgramId == TrainingProgramId);
                disciplinesCount = disciplinesPerPages.Count();
            }
            disciplinesPerPages =
                disciplinesPerPages.OrderBy(d => d.Id).ThenBy(d => d.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize).ToList();
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = disciplinesCount };
            var TrainingPrograms = await db.TrainingPrograms.Select(tp => new { Id = tp.Id, Name = tp.Name }).ToListAsync();
            TrainingPrograms.Add(new { Id = 0, Name = "Все программы" });
            IndexViewModel<Discipline> ivm = new IndexViewModel<Discipline>
            {
                PageInfo = pageInfo,
                Objects = disciplinesPerPages,
                Name = Name ?? "",
                TrainingPrograms = new SelectList(TrainingPrograms.OrderBy(tp => tp.Id), "Id", "Name"),
                SelectedTrainingProgram = TrainingProgramId ?? 0
            };
            return View(ivm);
            
        }

        public async Task<ActionResult> InterfaceForStudents()
        {
            int? GroupId = (await db.Users.Where(u => String.Equals(u.Email, User.Identity.Name)).FirstOrDefaultAsync()).GroupId;
            if (GroupId == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = await db.Groups.FindAsync(GroupId);
            if (group == null)
            {
                return HttpNotFound();
            }
            int? TrainingProgramId = group.TrainingProgramId;
            ViewBag.TrainingProgram = await db.TrainingPrograms.Where(t => t.Id == TrainingProgramId).FirstAsync();
            IEnumerable<Discipline> disciplines1 = await db.Disciplines.Where(d => d.TrainingProgramId == group.TrainingProgramId).ToListAsync();
            /*var users = (from u in db.Users
                         where (u.GroupId == GroupId)
                         select u).Distinct().ToList();
            IQueryable<Discipline> disciplines1 = (from d in db.Disciplines
                                                   join u in db.Users.Where(u=> u.GroupId == GroupId) on d.Id equals u.DisciplineId
                                                   join f in db.ApplicationFiles on u.Id equals f.TeacherId
                                                   where (u.GroupId == GroupId)
                                                   select d).Distinct();*/


            
            //IEnumerable<ApplicationUser> users = db.Users.Where(u=>u.GroupId == GroupId).ToList();
            IEnumerable<GroupDiscipline> disciplines = await db.Disciplines
                .Where(d =>d.TrainingProgramId == group.TrainingProgramId).Select(d => new GroupDiscipline(){Id = d.Id, Name = d.Name, Teacher = d.Teachers.Where(t=>t.GroupId == GroupId).FirstOrDefault()}).ToListAsync();


            /*IEnumerable<Discipline> disciplines = (from d in db.Disciplines
                                                   join u in db.Users on d.Id equals u.DisciplineId
                                                   join f in db.ApplicationFiles on u.Id equals f.TeacherId
                                                   where (d.TrainingProgramId == TrainingProgramId)
                                                   select d).Distinct().ToList();*/
            return View(disciplines);
        }
        

        
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discipline discipline = await db.Disciplines.FindAsync(id);
            if (discipline == null)
            {
                return HttpNotFound();
            }
            return PartialView(discipline);
        }
        [HttpGet]
        public async Task<ActionResult> AddFile(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discipline discipline = await db.Disciplines.FindAsync(id);
            if (discipline == null)
            {
                return HttpNotFound();
            }
            ViewBag.DisciplineId = id;
            return PartialView(discipline);
        }
        [HttpPost]
        public async Task AddFile(int? disciplineId, string displayName, string comment, HttpPostedFileBase file)
        {
            //напиши обработку что не null
            //if (disciplineId == null) { return new HttpStatusCodeResult(HttpStatusCode.BadRequest); }
            if (file != null && disciplineId != null)
            {
                string fileName = HashCreator.GetHashString(DateTime.Now.ToString() + Path.GetFileName(file.FileName)) + Path.GetExtension(file.FileName);//Path.GetFileName(file.FileName);
              //  if (!db.ApplicationFiles.Any(f => String.Equals(f.DisplayName, displayName)) && !db.ApplicationFiles.Any(f => String.Equals(f.FileName, fileName)))
              //  {
                    string TeacherId = (await db.Users.Where(u => String.Equals(u.Email, User.Identity.Name)).FirstOrDefaultAsync()).Id;
                    ApplicationFile newfile = new ApplicationFile() { DisplayName = displayName, FileName = fileName, UploadDate = DateTime.Now, DisciplineId = (int)disciplineId, TeacherId = TeacherId };
                    db.ApplicationFiles.Add(newfile);
                    await db.SaveChangesAsync();

                    DirectoryInfo dirInfo = new DirectoryInfo(Server.MapPath("~/Files/Disciplines/" + disciplineId));
                    if (!dirInfo.Exists)
                    {
                        dirInfo.Create();
                    }
                    file.SaveAs(Server.MapPath("~/Files/Disciplines/" + disciplineId + "/" + fileName));
                    
                //}
            }
        }
        
        public async Task<ActionResult> Create()
        {
            ViewBag.TrainingProgramId = new SelectList(db.TrainingPrograms, "Id", "Name");
            ViewBag.Teachers = await db.Users.Where(u => u.DisciplineId == null && u.Roles.Any(r => r.RoleId == "306641c0-2a3e-4471-90be-dbafa9157e20")).ToListAsync();
            string referrer = HttpContext.Request.UrlReferrer == null ? "/Disciplines" : HttpContext.Request.UrlReferrer.PathAndQuery;
            ViewBag.Referrer = referrer;
            return View();
        }
        
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Discipline discipline, String[] selectedTeachers, string referrer = "")
        {
            if (ModelState.IsValid)
            {
                discipline.Teachers.Clear();
                if (selectedTeachers != null)
                {
                    foreach (var u in db.Users.Where(us => selectedTeachers.Contains(us.Id)))
                    {
                        discipline.Teachers.Add(u);
                    }
                }
                db.Disciplines.Add(discipline);
                await db.SaveChangesAsync();
                //return RedirectToAction("Index");
                return Redirect(referrer);
            }

            ViewBag.TrainingProgramId = new SelectList(db.TrainingPrograms, "Id", "Name", discipline.TrainingProgramId);
            ViewBag.Teachers = await db.Users.Where(u => u.DisciplineId == null && u.Roles.Any(r => r.RoleId == "306641c0-2a3e-4471-90be-dbafa9157e20")).ToListAsync();
            ViewBag.Referrer = referrer;
            return View(discipline);
        }
        
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discipline discipline = await db.Disciplines.FindAsync(id);
            if (discipline == null)
            {
                return HttpNotFound();
            }
            ViewBag.TrainingProgramId = new SelectList(db.TrainingPrograms, "Id", "Name", discipline.TrainingProgramId);
            ViewBag.Teachers = await db.Users.Where(u => (u.DisciplineId == id || u.DisciplineId == null) && u.Roles.Any(r => r.RoleId == "306641c0-2a3e-4471-90be-dbafa9157e20")).ToListAsync();
            string referrer = HttpContext.Request.UrlReferrer == null ? "/Disciplines" : HttpContext.Request.UrlReferrer.PathAndQuery;
            ViewBag.Referrer = referrer;
            return View(discipline);
        }
        
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Discipline discipline, String[] selectedTeachers, string referrer = "")
        {
            if (ModelState.IsValid)
            {
                Discipline newdiscipline = await db.Disciplines.FindAsync(discipline.Id);
                newdiscipline.Name = discipline.Name;
                newdiscipline.Teachers.Clear();
                if (selectedTeachers != null)
                {
                    foreach (var u in db.Users.Where(us => selectedTeachers.Contains(us.Id)))
                    {
                        newdiscipline.Teachers.Add(u);
                    }
                }
                db.Entry(newdiscipline).State = EntityState.Modified;
                await db.SaveChangesAsync();
                //return RedirectToAction("Index");
                return Redirect(referrer);
            }
            ViewBag.TrainingProgramId = new SelectList(db.TrainingPrograms, "Id", "Name", discipline.TrainingProgramId);
            ViewBag.Teachers = await db.Users.Where(u => (u.DisciplineId == discipline.Id || u.DisciplineId == null) && u.Roles.Any(r => r.RoleId == "306641c0-2a3e-4471-90be-dbafa9157e20")).ToListAsync();
            ViewBag.Referrer = referrer;
            return View(discipline);
        }
        
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Discipline discipline = await db.Disciplines.FindAsync(id);
            if (discipline == null)
            {
                return HttpNotFound();
            }
            ViewBag.Teachers = await db.Users.Where(u => u.DisciplineId == id).ToListAsync();
            string referrer = HttpContext.Request.UrlReferrer == null ? "/Disciplines" : HttpContext.Request.UrlReferrer.PathAndQuery;
            ViewBag.Referrer = referrer;
            return View(discipline);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id, string referrer = "")
        {
            //https://professorweb.ru/my/entity-framework/6/level2/2_3.php
            Discipline discipline = await db.Disciplines.Include(d => d.Teachers).FirstOrDefaultAsync(d=>d.Id == id);
            db.Disciplines.Remove(discipline);
            await db.SaveChangesAsync();
            return Redirect(referrer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
