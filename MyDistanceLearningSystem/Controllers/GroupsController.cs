﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyDistanceLearningSystem.Models;
using PagedList.Mvc;
using PagedList;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace MyDistanceLearningSystem.Controllers
{
    [Authorize]
    public class GroupsController : Controller
    {
        public ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }
        private ApplicationDbContext db = new ApplicationDbContext();

        public ActionResult ChatWithWebSockets()
        {
            return View();
        }
        
        public async Task<ActionResult> Index(string Name, int? TrainingProgramId, int? ItemCountPerPage, int page = 1)
        {
            int groupsCount;
            IEnumerable<Group> groupsPerPages;
            int pageSize = ItemCountPerPage != null ? (int)ItemCountPerPage : 5;
            if (User.IsInRole("admin"))
            {
                groupsCount = await db.Groups.CountAsync();
                groupsPerPages = await db.Groups.Include(g => g.TrainingProgram).ToListAsync();
            }else
            {
                int? GroupId = (await db.Users.Where(u => String.Equals(u.Email, User.Identity.Name)).FirstOrDefaultAsync()).GroupId;
                groupsCount = await db.Groups.Where(g=>g.Id == GroupId).CountAsync();
                groupsPerPages = await db.Groups.Where(g => g.Id == GroupId).Include(g => g.TrainingProgram).ToListAsync();
            }
            if (!String.IsNullOrEmpty(Name))
            {
                groupsPerPages = groupsPerPages.Where(s => s.Name.IndexOf(Name) != -1);
                groupsCount = groupsPerPages.Count();
            }
            if (TrainingProgramId != null && TrainingProgramId != 0)
            {
                groupsPerPages = groupsPerPages.Where(s => s.TrainingProgramId == TrainingProgramId);
                groupsCount = groupsPerPages.Count();
            }
            groupsPerPages =
                groupsPerPages.OrderBy(g => g.Id).ThenBy(g => g.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize).ToList();
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = groupsCount };
            var TrainingPrograms = await db.TrainingPrograms.Select(tp => new { Id = tp.Id, Name = tp.Name}).ToListAsync();
            TrainingPrograms.Add(new { Id = 0, Name = "Все программы"});
            IndexViewModel<Group> ivm = new IndexViewModel<Group>
            {
                PageInfo = pageInfo,
                Objects = groupsPerPages,
                Name = Name ?? "",
                TrainingPrograms = new SelectList(TrainingPrograms.OrderBy(tp=>tp.Id), "Id", "Name"),
                SelectedTrainingProgram = TrainingProgramId ?? 0
            };
            return View(ivm);


           /* int pageSize = 10;
            int pageNumber = (page ?? 1);
            var groups = db.Groups.Include(g => g.TrainingProgram);
            ViewBag.TrainingProgramId = new SelectList(db.TrainingPrograms, "Id", "Name", group.TrainingProgramId);
            return View(groups.ToList().ToPagedList(pageNumber, pageSize));*/
        }

        public async Task<ActionResult> StudentsList(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = await db.Groups.FindAsync(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            IEnumerable<ApplicationUser> students = await db.Users.Where(u => u.Roles.Any(r => r.RoleId == "0ccde516-98df-4ea6-9be2-efd467a98d1d") && u.GroupId == id).ToListAsync();
            ViewBag.Disciplines = await db.Disciplines.Where(d=>d.TrainingProgramId == group.TrainingProgramId).ToListAsync();
            return View(students);
        }
        
        public async Task<ActionResult> StudentsInfo(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ApplicationUser user = await UserManager.FindByIdAsync(id);
            if (user == null)
            {
                return HttpNotFound();
            }
            ViewBag.Teacher = await db.Users.Where(u => String.Equals(u.Email, User.Identity.Name)).Include(u=>u.Discipline).FirstOrDefaultAsync();
            return PartialView(user);
        }

        public async Task<ActionResult> GroupChat()
        {
           /* ViewBag.UserName =
            string TeacherId = db.Users.Where(u => String.Equals(u.Email, User.Identity.Name)).FirstOrDefault().Id;*/

            ApplicationUser user = await UserManager.FindByEmailAsync(User.Identity.Name);
            return View(user);
        }

        // GET: Groups/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = await db.Groups.FindAsync(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            return PartialView(group);
        }
        
        public async Task<ActionResult> Create()
        {
            ViewBag.TrainingProgramId = new SelectList(db.TrainingPrograms.OrderBy(t=>t.Name), "Id", "Name");
            ViewBag.Disciplines = await (from d in db.Disciplines
                                   join u in db.Users on d.Id equals u.DisciplineId
                                   where d.TrainingProgramId == db.TrainingPrograms.OrderBy(t => t.Name).FirstOrDefault().Id
                                   select d).Distinct().ToListAsync();
            ViewBag.Students = await db.Users.Where(u => u.GroupId == null && u.Roles.Any(r => r.RoleId == "0ccde516-98df-4ea6-9be2-efd467a98d1d")).ToListAsync();
            string referrer = HttpContext.Request.UrlReferrer == null ? "/Groups" : HttpContext.Request.UrlReferrer.PathAndQuery;
            ViewBag.Referrer = referrer;
            return View();
        }
        
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(Group group, String[] selectedStudents, List<string> teachers, string referrer = "")
        {
            if (ModelState.IsValid)
            {
                group.Students.Clear();
                if (selectedStudents != null)
                {
                    foreach (var u in db.Users.Where(us => selectedStudents.Contains(us.Id)))
                    {
                        group.Students.Add(u);
                    }
                }
                db.Groups.Add(group);
                await db.SaveChangesAsync();
                if (teachers != null)
                {
                    foreach (string t in teachers.Where(t => !String.IsNullOrEmpty(t)))
                    {
                        
                        ApplicationUser user = await UserManager.FindByIdAsync(t);
                        user.GroupId = group.Id;
                        IdentityResult result = await UserManager.UpdateAsync(user);
                        
                    }
                }
                //return RedirectToAction("Index", new { page = page });
                return Redirect(referrer);
            }
            ViewBag.TrainingProgramId = new SelectList(db.TrainingPrograms, "Id", "Name", group.TrainingProgramId);
            ViewBag.Students = await db.Users.Where(u => u.GroupId == null && u.Roles.Any(r => r.RoleId == "0ccde516-98df-4ea6-9be2-efd467a98d1d")).ToListAsync();
            ViewBag.Referrer = referrer;
            return View(group);
        }
        
        public async Task<ActionResult> Edit(int? id)
        {
            
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = await db.Groups.FindAsync(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            string referrer = HttpContext.Request.UrlReferrer == null ? "/Groups" : HttpContext.Request.UrlReferrer.PathAndQuery;
            int trainingProgramId = (int)group.TrainingProgramId;
            ViewBag.Disciplines = await (from d in db.Disciplines
                                            join u in db.Users on d.Id equals u.DisciplineId
                                            where d.TrainingProgramId == trainingProgramId
                                            select d).Distinct().ToListAsync();

            ViewBag.TrainingProgramId = new SelectList(db.TrainingPrograms, "Id", "Name", group.TrainingProgramId);
            ViewBag.Students = await db.Users.Where(u => (u.GroupId == id || u.GroupId == null) && u.Roles.Any(r => r.RoleId == "0ccde516-98df-4ea6-9be2-efd467a98d1d")).ToListAsync();
            ViewBag.Referrer = referrer;
            return View(group);
        }
        
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(Group group, String[] selectedStudents, List<string> teachers, string referrer = "")
        {
            if (ModelState.IsValid)
            {
                Group newgroup = await db.Groups.FindAsync(group.Id);
                newgroup.Name = group.Name;
                newgroup.TrainingProgramId = group.TrainingProgramId;
                newgroup.Students.Clear();
                if (selectedStudents != null)
                {
                    foreach (var u in db.Users.Where(us => selectedStudents.Contains(us.Id)))
                    {
                        newgroup.Students.Add(u);
                    }
                }
                db.Entry(newgroup).State = EntityState.Modified;
                await db.SaveChangesAsync();
                if (teachers != null)
                {
                    foreach (string t in teachers.Where(t=>!String.IsNullOrEmpty(t)))
                    {
                        ApplicationUser user = await UserManager.FindByIdAsync(t);
                        user.GroupId = group.Id;
                        IdentityResult result = await UserManager.UpdateAsync(user);
                    }
                }
                //return RedirectToAction("Index", new { page = page });
                return Redirect(referrer);
            }
            int trainingProgramId = (int)group.TrainingProgramId;
            ViewBag.Disciplines = await (from d in db.Disciplines
                                   join u in db.Users on d.Id equals u.DisciplineId
                                   where d.TrainingProgramId == trainingProgramId
                                   select d).Distinct().ToListAsync();
            ViewBag.TrainingProgramId = new SelectList(db.TrainingPrograms, "Id", "Name", group.TrainingProgramId);
            ViewBag.Students = await db.Users.Where(u => (u.GroupId == group.Id || u.GroupId == null) && u.Roles.Any(r => r.RoleId == "0ccde516-98df-4ea6-9be2-efd467a98d1d")).ToListAsync();
            ViewBag.Referrer = referrer;
            return View(group);
        }

        [HttpGet]
        public async Task<ActionResult> BindTeachers(int? groupId, int trainingProgramId, bool isGroupCreate = false)
        {
            if (groupId == null && isGroupCreate == false)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            List<Discipline> disciplines = await (from d in db.Disciplines
                                                  join u in db.Users on d.Id equals u.DisciplineId
                                                  where d.TrainingProgramId == trainingProgramId
                                                  select d).Distinct().ToListAsync();
            ViewBag.GroupId = groupId;
            ViewBag.TrainingProgram = (await db.TrainingPrograms.FindAsync(trainingProgramId)).Name;
            return PartialView(disciplines);
        }
        [HttpPost]
        public async Task BindTeachers(List<string> Teachers, int? groupId)
        {

            foreach(string t in Teachers)
            {
                ApplicationUser user = await UserManager.FindByIdAsync(t);
                user.GroupId = groupId;
                IdentityResult result = await UserManager.UpdateAsync(user);
            }
        }
        
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Group group = await db.Groups.FindAsync(id);
            if (group == null)
            {
                return HttpNotFound();
            }
            ViewBag.Students = await db.Users.Where(u => u.GroupId == id).ToListAsync();
            string referrer = HttpContext.Request.UrlReferrer == null ? "/Groups" : HttpContext.Request.UrlReferrer.PathAndQuery;
            ViewBag.Referrer = referrer;
            return View(group);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id, string referrer = "")
        {
            //https://professorweb.ru/my/entity-framework/6/level2/2_3.php
            Group group = await db.Groups.Include(g => g.Students).FirstOrDefaultAsync(g => g.Id == id);
            db.Groups.Remove(group);
            await db.SaveChangesAsync();
            return Redirect(referrer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
