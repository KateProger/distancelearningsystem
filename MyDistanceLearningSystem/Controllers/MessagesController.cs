﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyDistanceLearningSystem.Models;
using System.IO;
using MyDistanceLearningSystem.Functions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace MyDistanceLearningSystem.Controllers
{
    public class MessagesController : Controller
    {
        public ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }
        private ApplicationDbContext db = new ApplicationDbContext();
        // GET: Messages
       /* public ActionResult MessagesList()
        {
            ApplicationUser user = UserManager.FindByEmail(User.Identity.Name);
            IEnumerable<Message> messages = db.Messages.Where(m => m.ReceiverId == user.Id).Include(m => m.Sender).OrderByDescending(m => m.UploadDate).ToList();
            foreach (Message message in messages)
            {
                message.IsRead = true;
                db.Entry(message).State = EntityState.Modified;
                db.SaveChanges();
            }
            return View(messages);
        }*/
        public async Task<ActionResult> MessagesList(string Name, int? ItemCountPerPage, int page = 1)
        {
            ApplicationUser user = await UserManager.FindByEmailAsync(User.Identity.Name);
            IEnumerable<Message> messages = await db.Messages.Where(m => m.ReceiverId == user.Id).Include(m => m.Sender).OrderByDescending(m => m.UploadDate).ToListAsync();
            foreach (Message message in messages.Where(m=>m.IsRead == false))
            {
                message.IsRead = true;
                db.Entry(message).State = EntityState.Modified;
                db.SaveChanges();
            }

            int messagesCount;
            IEnumerable<Message> messagesPerPages;
            int pageSize = ItemCountPerPage != null ? (int)ItemCountPerPage : 5;
            
            messagesCount = messages.Count();
            messagesPerPages = messages;
            
            if (!String.IsNullOrEmpty(Name))
            {
                messagesPerPages = messagesPerPages.Where(s => s.Name.IndexOf(Name) != -1);
                messagesCount = messagesPerPages.Count();
            }

            messagesPerPages =
                messagesPerPages.OrderByDescending(m => m.UploadDate).ThenBy(g => g.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize).ToList();
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = messagesCount };
            IndexViewModel<Message> ivm = new IndexViewModel<Message>
            {
                PageInfo = pageInfo,
                Objects = messagesPerPages,
                Name = Name ?? ""
            };
            return View(ivm);
        }
        [HttpGet]
        public async Task<ActionResult> Create(string Id)
        {
            if(Id == null) return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            /*ApplicationUser user = UserManager.FindById(receiverId);
            if (user == null)  return HttpNotFound();*/
            ViewBag.ReceiverId = Id;
            var Sender = await UserManager.FindByEmailAsync(User.Identity.Name);
            ViewBag.SenderId = Sender.Id;
            ViewBag.SenderName = Sender.Name;
            return PartialView();
        }
        public ActionResult UnreadMesagesCountSelect()
        {
            ApplicationUser user = db.Users.Where(u => String.Equals(u.Email, User.Identity.Name)).FirstOrDefault();

            ViewBag.UnreadMesagesCount = user != null ? db.Messages.Where(m => m.ReceiverId == user.Id && m.IsRead == false).Count() : 0;

            return PartialView();
        }
        /*public ActionResult Create1()
        {
            
            return View();
        }*/
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = await db.Messages.FindAsync(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            return PartialView(message);
        }
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Message message = await db.Messages.FindAsync(id);
            if (message == null)
            {
                return HttpNotFound();
            }
            
            db.Messages.Remove(message);
            await db.SaveChangesAsync();
            return RedirectToAction("MessagesList");
            
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}