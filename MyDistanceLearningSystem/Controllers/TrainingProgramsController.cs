﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MyDistanceLearningSystem.Models;

namespace MyDistanceLearningSystem.Controllers
{
    [Authorize(Roles = "admin")]
    public class TrainingProgramsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        
        public async Task<ActionResult> Index(string Name, int? ItemCountPerPage, int page = 1)
        {
            int pageSize = ItemCountPerPage != null ? (int)ItemCountPerPage : 5;
            int programsCount = await db.TrainingPrograms.CountAsync();
            IEnumerable<TrainingProgram> programsPerPages = await db.TrainingPrograms.ToListAsync();
            if (!String.IsNullOrEmpty(Name))
            {
                programsPerPages = programsPerPages.Where(s => s.Name.IndexOf(Name) != -1);
                programsCount = programsPerPages.Count();
            }
            programsPerPages =
                programsPerPages.OrderBy(g => g.Id).ThenBy(g => g.Name)
                .Skip((page - 1) * pageSize)
                .Take(pageSize).ToList();
            PageInfo pageInfo = new PageInfo { PageNumber = page, PageSize = pageSize, TotalItems = programsCount };
            IndexViewModel<TrainingProgram> ivm = new IndexViewModel<TrainingProgram>
            {
                PageInfo = pageInfo,
                Objects = programsPerPages,
                Name = Name ?? ""
            };
            return View(ivm);
        }
        
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingProgram trainingProgram = await db.TrainingPrograms.FindAsync(id);
            if (trainingProgram == null)
            {
                return HttpNotFound();
            }
            return PartialView(trainingProgram);
        }
        
        
        public async Task<ActionResult> Create()
        {
            ViewBag.Disciplines = await db.Disciplines.Where(d => d.TrainingProgramId == null).ToListAsync();
            ViewBag.Groups = await db.Groups.Where(g => g.TrainingProgramId == null).ToListAsync();
            string referrer = HttpContext.Request.UrlReferrer == null ? "/TrainingPrograms" : HttpContext.Request.UrlReferrer.PathAndQuery;
            ViewBag.Referrer = referrer;
            return View();
        }
        
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(TrainingProgram trainingProgram, int[] selectedDisciplines, int[] selectedGroups, string referrer = "")
        {
            if (ModelState.IsValid)
            {
                TrainingProgram newtrainingProgram = new TrainingProgram();
                newtrainingProgram.Name = trainingProgram.Name;
                if (selectedDisciplines != null)
                {
                    foreach (var d in db.Disciplines.Where(di => selectedDisciplines.Contains(di.Id)))
                    {
                        newtrainingProgram.Disciplines.Add(d);
                    }
                }
                if (selectedGroups != null)
                {
                    foreach (var g in db.Groups.Where(gr => selectedGroups.Contains(gr.Id)))
                    {
                        newtrainingProgram.Groups.Add(g);
                    }
                }
                db.TrainingPrograms.Add(newtrainingProgram);
                await db.SaveChangesAsync();
                //return RedirectToAction("Index");
                return Redirect(referrer);
            }
            ViewBag.Referrer = referrer;
            return View(trainingProgram);
            
        }
        [HttpGet]
        public ActionResult CreateDiscipline(int? trainingProgramId, bool isTrainingProgramCreate = false)
        {
            if (trainingProgramId == null && isTrainingProgramCreate == false)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.TrainingProgramId = trainingProgramId;
            return PartialView();
        }
        
        [HttpPost]
        public async Task<JsonResult> CreateDiscipline(Discipline d)
        {
            db.Disciplines.Add(d);
            await db.SaveChangesAsync();
            return Json(d);
        }
        [HttpGet]
        public ActionResult CreateGroup(int? trainingProgramId, bool isTrainingProgramCreate = false)
        {
            if (trainingProgramId == null && isTrainingProgramCreate == false)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ViewBag.TrainingProgramId = trainingProgramId;
            return PartialView();
        }
        [HttpPost]
        public async Task<JsonResult> CreateGroup(Group g)
        {

            db.Groups.Add(g);
            await db.SaveChangesAsync();
            return Json(g);
        }
        
       
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingProgram trainingProgram = await db.TrainingPrograms.FindAsync(id);
            if (trainingProgram == null)
            {
                return HttpNotFound();
            }
            ViewBag.Disciplines = await db.Disciplines.Where(d=>d.TrainingProgramId == id || d.TrainingProgramId == null).ToListAsync();
            ViewBag.Groups = await db.Groups.Where(g => g.TrainingProgramId == id || g.TrainingProgramId == null).ToListAsync();
            string referrer = HttpContext.Request.UrlReferrer == null ? "/TrainingPrograms" : HttpContext.Request.UrlReferrer.PathAndQuery;
            ViewBag.Referrer = referrer;
            return View(trainingProgram);
        }
       
        // Чтобы защититься от атак чрезмерной передачи данных, включите определенные свойства, для которых следует установить привязку. Дополнительные 
        // сведения см. в статье http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(TrainingProgram trainingProgram, int[] selectedDisciplines, int[] selectedGroups, string referrer = "")
        {
            if (ModelState.IsValid)
            {
                TrainingProgram newtrainingProgram = await db.TrainingPrograms.FindAsync(trainingProgram.Id);
                newtrainingProgram.Name = trainingProgram.Name;
                newtrainingProgram.Disciplines.Clear();
                if (selectedDisciplines != null)
                {
                    foreach (var d in db.Disciplines.Where(di => selectedDisciplines.Contains(di.Id)))
                    {
                        newtrainingProgram.Disciplines.Add(d);
                    }
                }
                newtrainingProgram.Groups.Clear();
                if (selectedGroups != null)
                {
                    foreach (var g in db.Groups.Where(gr => selectedGroups.Contains(gr.Id)))
                    {
                        newtrainingProgram.Groups.Add(g);
                    }
                }
                db.Entry(newtrainingProgram).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return Redirect(referrer);
            }
            ViewBag.Referrer = referrer;
            return View(trainingProgram);
        }
        
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            TrainingProgram trainingProgram = await db.TrainingPrograms.FindAsync(id);
            if (trainingProgram == null)
            {
                return HttpNotFound();
            }
            ViewBag.Disciplines = await db.Disciplines.Where(d => d.TrainingProgramId == id).ToListAsync();
            ViewBag.Groups = await db.Groups.Where(g => g.TrainingProgramId == id).ToListAsync();
            string referrer = HttpContext.Request.UrlReferrer == null ? "/TrainingPrograms" : HttpContext.Request.UrlReferrer.PathAndQuery;
            ViewBag.Referrer = referrer;
            return View(trainingProgram);
        }
        
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id, string referrer = "")
        {
            //TrainingProgram trainingProgram = await db.TrainingPrograms.FindAsync(id);
            //https://professorweb.ru/my/entity-framework/6/level2/2_3.php
            TrainingProgram trainingProgram = await db.TrainingPrograms.Include(t => t.Disciplines).Include(t => t.Groups).FirstOrDefaultAsync(t => t.Id == id);
            db.TrainingPrograms.Remove(trainingProgram);
            await db.SaveChangesAsync();
            return Redirect(referrer);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public async Task<ActionResult> DisciplineList()
        {
            return PartialView(await db.Disciplines.ToListAsync());
        }
    }
}
