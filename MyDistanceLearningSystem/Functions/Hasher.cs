﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Security.Cryptography;
using System.Text;

namespace MyDistanceLearningSystem.Functions
{
    public static class HashCreator
    {

        public static string GetHashString(string s)
        {
            MD5 md5Hasher = MD5.Create();
            
            byte[] data = md5Hasher.ComputeHash(Encoding.Default.GetBytes(s));
            
            StringBuilder hash = new StringBuilder();
            
            for (int i = 0; i < data.Length; i++)
            {
                hash.Append(data[i].ToString("x2"));
            }
            return hash.ToString();
        }
    }
}