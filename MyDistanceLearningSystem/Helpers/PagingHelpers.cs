﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MyDistanceLearningSystem.Models;
using System.Text;
using System.Web.Mvc;

namespace MyDistanceLearningSystem.Helpers
{
    public static class PagingHelpers
    {
        public static MvcHtmlString PageLinqs(this HtmlHelper html, PageInfo pageInfo, string Name,int TrainingProgramId, Func<string, int, int, int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= pageInfo.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(Name,TrainingProgramId, pageInfo.PageSize, i));
                tag.InnerHtml = i.ToString();
                if (i == pageInfo.PageNumber)
                {
                    tag.AddCssClass("selected");
                    tag.AddCssClass("btn-primary");
                }
                tag.AddCssClass("btn btn-default");
                result.Append(tag.ToString());

            }
            return MvcHtmlString.Create(result.ToString());
        }
        public static MvcHtmlString PageLinqs(this HtmlHelper html, PageInfo pageInfo, string Name, int DisciplineId, string TeacherId, Func<string, int, string, int, int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= pageInfo.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(Name, DisciplineId, TeacherId, pageInfo.PageSize, i));
                tag.InnerHtml = i.ToString();
                if (i == pageInfo.PageNumber)
                {
                    tag.AddCssClass("selected");
                    tag.AddCssClass("btn-primary");
                }
                tag.AddCssClass("btn btn-default");
                result.Append(tag.ToString());

            }
            return MvcHtmlString.Create(result.ToString());
        }
        public static MvcHtmlString PageLinqs(this HtmlHelper html, PageInfo pageInfo, string Name, Func<string, int, int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= pageInfo.TotalPages; i++)
            {
                TagBuilder tag = new TagBuilder("a");
                tag.MergeAttribute("href", pageUrl(Name, pageInfo.PageSize, i));
                tag.InnerHtml = i.ToString();
                if (i == pageInfo.PageNumber)
                {
                    tag.AddCssClass("selected");
                    tag.AddCssClass("btn-primary");
                }
                tag.AddCssClass("btn btn-default");
                result.Append(tag.ToString());

            }
            return MvcHtmlString.Create(result.ToString());
        }
    }
}