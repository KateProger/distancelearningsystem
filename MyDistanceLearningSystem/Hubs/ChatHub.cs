﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using MyDistanceLearningSystem.Models;


namespace MyDistanceLearningSystem.Hubs
{
    public class ChatHub : Hub
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        static List<ChatUser> Users = new List<ChatUser>();

        // Отправка сообщений
        public void Send(string name, string message)
        {
            var item = Users.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            //Clients.All.addMessage(name, message);
            Clients.Group(item.GroupName).addMessage(name, message);

        }

        // Подключение нового пользователя
        public void Connect(string userName)
        {
            var id = Context.ConnectionId;
            string UserId = db.Users.Where(u => String.Equals(u.Email, Context.User.Identity.Name)).FirstOrDefault().Id;
            int GroupId = (int)db.Users.Where(u => String.Equals(u.Email, Context.User.Identity.Name)).FirstOrDefault().GroupId;

            if (!Users.Any(x => x.ConnectionId == id))
            {
                Users.Add(new ChatUser { ConnectionId = id, Name = userName, Id = UserId, GroupName = GroupId.ToString() });

                Groups.Add(Context.ConnectionId, GroupId.ToString());
                // Посылаем сообщение текущему пользователю
                Clients.Caller.onConnected(id, userName, Users);

                // Посылаем сообщение всем пользователям, кроме текущего
                //Clients.AllExcept(id).onNewUserConnected(id, userName);
                //Clients.Group(GroupId.ToString()).AllExcept(id).onNewUserConnected(id, userName);
                Clients.OthersInGroup(GroupId.ToString()).onNewUserConnected(id, userName);
            }
        }

        // Отключение пользователя
        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            var item = Users.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                Users.Remove(item);
                var id = Context.ConnectionId;
                Groups.Remove(Context.ConnectionId, item.GroupName);
                Clients.All.onUserDisconnected(id, item.Name);
            }

            return base.OnDisconnected(stopCalled);
        }
    }
}