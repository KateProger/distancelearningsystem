﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using MyDistanceLearningSystem.Models;


namespace MyDistanceLearningSystem.Hubs
{
    public class MessagesHub : Hub
    {

        private ApplicationDbContext db = new ApplicationDbContext();
        static List<ChatUser> Users = new List<ChatUser>();

        // Отправка сообщений
        public void Send(string senderId, string senderName, string messageName, string messageBody, string receiverId)
        {
            Message message = new Message() {Name = messageName, Body = messageBody, UploadDate= DateTime.Now, SenderId = senderId, ReceiverId = receiverId};
            db.Messages.Add(message);
            db.SaveChanges();
            var item = Users.FirstOrDefault(x => x.Id == receiverId);
            int unreadMessagesCount = db.Messages.Where(m => m.ReceiverId == receiverId && m.IsRead == false).Count();
            if (item != null)
            {
                Clients.Group(item.Id).addMessage(senderId, senderName, message.Id, messageName, messageBody, message.UploadDate.ToString(), unreadMessagesCount);
            }
        }

        // Подключение нового пользователя
        public void Connect(string userName)
        {
            var id = Context.ConnectionId;
            string UserId = db.Users.Where(u => String.Equals(u.Email, Context.User.Identity.Name)).FirstOrDefault().Id;
            if (!Users.Any(x => x.ConnectionId == id))
            {
                Users.Add(new ChatUser { ConnectionId = id, Name = userName, Id = UserId });
            }
            Groups.Add(Context.ConnectionId, UserId);
        }

        // Отключение пользователя
        public override System.Threading.Tasks.Task OnDisconnected(bool stopCalled)
        {
            var item = Users.FirstOrDefault(x => x.ConnectionId == Context.ConnectionId);
            if (item != null)
            {
                Users.Remove(item);
                Groups.Remove(Context.ConnectionId, item.Id);
            }

            return base.OnDisconnected(stopCalled);
        }
    }
}