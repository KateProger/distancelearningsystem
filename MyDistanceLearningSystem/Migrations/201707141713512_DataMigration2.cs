namespace MyDistanceLearningSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataMigration2 : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.AspNetUsers", name: "Discipline_Id", newName: "DisciplineId");
            RenameIndex(table: "dbo.AspNetUsers", name: "IX_Discipline_Id", newName: "IX_DisciplineId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.AspNetUsers", name: "IX_DisciplineId", newName: "IX_Discipline_Id");
            RenameColumn(table: "dbo.AspNetUsers", name: "DisciplineId", newName: "Discipline_Id");
        }
    }
}
