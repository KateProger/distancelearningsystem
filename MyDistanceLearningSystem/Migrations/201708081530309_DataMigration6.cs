namespace MyDistanceLearningSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataMigration6 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Libraries", "TrainingProgramId", "dbo.TrainingPrograms");
            DropIndex("dbo.Libraries", new[] { "TrainingProgramId" });
            AddColumn("dbo.Libraries", "DisplayName", c => c.String());
            AddColumn("dbo.Libraries", "FileName", c => c.String());
            AddColumn("dbo.Libraries", "UploadDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Libraries", "DisciplineId", c => c.Int(nullable: false));
            CreateIndex("dbo.Libraries", "DisciplineId");
            AddForeignKey("dbo.Libraries", "DisciplineId", "dbo.Disciplines", "Id", cascadeDelete: true);
            DropColumn("dbo.Libraries", "TrainingProgramId");
            DropColumn("dbo.Libraries", "Path");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Libraries", "Path", c => c.String());
            AddColumn("dbo.Libraries", "TrainingProgramId", c => c.Int(nullable: false));
            DropForeignKey("dbo.Libraries", "DisciplineId", "dbo.Disciplines");
            DropIndex("dbo.Libraries", new[] { "DisciplineId" });
            DropColumn("dbo.Libraries", "DisciplineId");
            DropColumn("dbo.Libraries", "UploadDate");
            DropColumn("dbo.Libraries", "FileName");
            DropColumn("dbo.Libraries", "DisplayName");
            CreateIndex("dbo.Libraries", "TrainingProgramId");
            AddForeignKey("dbo.Libraries", "TrainingProgramId", "dbo.TrainingPrograms", "Id", cascadeDelete: true);
        }
    }
}
