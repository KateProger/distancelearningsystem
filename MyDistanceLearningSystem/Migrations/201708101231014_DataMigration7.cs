namespace MyDistanceLearningSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataMigration7 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Libraries", newName: "Files");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Files", newName: "Libraries");
        }
    }
}
