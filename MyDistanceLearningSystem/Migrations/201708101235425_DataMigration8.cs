namespace MyDistanceLearningSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataMigration8 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Files", newName: "ApplicationFiles");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.ApplicationFiles", newName: "Files");
        }
    }
}
