namespace MyDistanceLearningSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataMigration9 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ApplicationFiles", "Comment", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.ApplicationFiles", "Comment");
        }
    }
}
