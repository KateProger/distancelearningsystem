// <auto-generated />
namespace MyDistanceLearningSystem.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class DataMigration10 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(DataMigration10));
        
        string IMigrationMetadata.Id
        {
            get { return "201708141946358_DataMigration10"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
