namespace MyDistanceLearningSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataMigration10 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HomeWork",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DisplayName = c.String(),
                        FileName = c.String(),
                        Comment = c.String(),
                        UploadDate = c.DateTime(nullable: false),
                        DisciplineId = c.Int(nullable: false),
                        StudentId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Disciplines", t => t.DisciplineId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.StudentId)
                .Index(t => t.DisciplineId)
                .Index(t => t.StudentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.HomeWork", "StudentId", "dbo.AspNetUsers");
            DropForeignKey("dbo.HomeWork", "DisciplineId", "dbo.Disciplines");
            DropIndex("dbo.HomeWork", new[] { "StudentId" });
            DropIndex("dbo.HomeWork", new[] { "DisciplineId" });
            DropTable("dbo.HomeWork");
        }
    }
}
