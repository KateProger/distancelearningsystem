namespace MyDistanceLearningSystem.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataMigration11 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.HomeWork", "DisciplineId", "dbo.Disciplines");
            DropForeignKey("dbo.HomeWork", "StudentId", "dbo.AspNetUsers");
            DropIndex("dbo.HomeWork", new[] { "DisciplineId" });
            DropIndex("dbo.HomeWork", new[] { "StudentId" });
            AddColumn("dbo.ApplicationFiles", "IsHomeWork", c => c.Boolean(nullable: false));
            DropTable("dbo.HomeWork");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.HomeWork",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DisplayName = c.String(),
                        FileName = c.String(),
                        Comment = c.String(),
                        UploadDate = c.DateTime(nullable: false),
                        DisciplineId = c.Int(nullable: false),
                        StudentId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id);
            
            DropColumn("dbo.ApplicationFiles", "IsHomeWork");
            CreateIndex("dbo.HomeWork", "StudentId");
            CreateIndex("dbo.HomeWork", "DisciplineId");
            AddForeignKey("dbo.HomeWork", "StudentId", "dbo.AspNetUsers", "Id");
            AddForeignKey("dbo.HomeWork", "DisciplineId", "dbo.Disciplines", "Id", cascadeDelete: true);
        }
    }
}
