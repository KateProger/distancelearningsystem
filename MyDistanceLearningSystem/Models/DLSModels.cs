﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace MyDistanceLearningSystem.Models
{
    
    public class TrainingProgram
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 30 символов")]
        [Display(Name = "Название программы")]
        public string Name { get; set; }
        
        [Display(Name = "Дисциплины")]
        public virtual ICollection<Discipline> Disciplines { get; set; }

        [Display(Name = "Группы")]
        public virtual ICollection<Group> Groups { get; set; }
        public TrainingProgram()
        {
            Disciplines = new List<Discipline>();
            Groups = new List<Group>();
        }
    }
    public class Discipline
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 30 символов")]
        [Display(Name = "Название дисциплины")]
        public string Name { get; set; }

        [Display(Name = "Программа")]
        public int? TrainingProgramId { get; set; }
        [Display(Name = "Программа")]
        public virtual TrainingProgram TrainingProgram { get; set; }

        [Display(Name = "Преподаватели")]
        public virtual ICollection<ApplicationUser> Teachers { get; set; }
        public Discipline()
        {
            Teachers = new List<ApplicationUser>();
        }
    }
    public class Group
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 30 символов")]
        [Display(Name = "Название")]
        public string Name { get; set; }

        [Display(Name = "Программа")]
        public int? TrainingProgramId { get; set; }
        [Display(Name = "Программа")]
        public TrainingProgram TrainingProgram { get; set; }

        [Display(Name = "Студенты")]
        public virtual ICollection<ApplicationUser> Students { get; set; }
        public Group()
        {
            Students = new List<ApplicationUser>();
        }
    }
    public class ApplicationFile
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 30 символов")]
        [Display(Name = "Отображаемое название")]
        public string DisplayName { get; set; }

        [Display(Name = "Название файла")]
        public string FileName { get; set; }

        [Display(Name = "Комментарий")]
        public string Comment { get; set; }

        [Display(Name = "Дата загрузки")]
        [DataType(DataType.DateTime)]
        public DateTime UploadDate { get; set; }

        public int DisciplineId { get; set; }
        public virtual Discipline Discipline { get; set; }
        public String TeacherId { get; set; }
        public virtual ApplicationUser Teacher { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool IsHomeWork { get; set; }
    }
    public class Message
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }

        [Required]
        [StringLength(30, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 30 символов")]
        [Display(Name = "Название сообщения")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "Сообщение")]
        public string Body { get; set; }

        [Display(Name = "Дата загрузки")]
        [DataType(DataType.DateTime)]
        public DateTime UploadDate { get; set; }

        public String SenderId { get; set; }
        public ApplicationUser Sender { get; set; }
        public String ReceiverId { get; set; }
        public ApplicationUser Receiver { get; set; }

        [HiddenInput(DisplayValue = false)]
        public bool IsRead { get; set; }
    }
    public class PageInfo
    {
        public int PageNumber { get; set; }
        public int PageSize { get; set; }
        public int TotalItems { get; set; }
        public int TotalPages
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / PageSize); }
        }
    }
    public class IndexViewModel<T>
    {
        public IEnumerable<T> Objects { get; set; }
        public PageInfo PageInfo { get; set; }
        public string Name { get; set; }
        public SelectList TrainingPrograms { get; set; }
        public int SelectedTrainingProgram { get; set; }
        public int SelectedDiscipline { get; set; }
        public string SelectedTeacher { get; set; }

    }
    public class GroupDiscipline
    {
        [HiddenInput(DisplayValue = false)]
        public int Id { get; set; }
        
        [Display(Name = "Название дисциплины")]
        public string Name { get; set; }

        [Display(Name = "Программа")]
        public int TrainingProgramId { get; set; }
        [Display(Name = "Программа")]
        public TrainingProgram TrainingProgram { get; set; }

        [Display(Name = "Преподаватель")]
        public ApplicationUser Teacher { get; set; }
        public GroupDiscipline()
        {
            Teacher = new ApplicationUser();
        }
    }
    public class ChatUser
    {
        public string Id { get; set; }
        public string ConnectionId { get; set; }
        public string Name { get; set; }
        public string GroupName { get; set; }
    }
    public class EditModel
    {
        [Display(Name = "Идентификатор пользователя")]
        public string Id { get; set; }

        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Display(Name = "Фамилия")]

        public string Surname { get; set; }
        [Display(Name = "Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

       /* [Display(Name = "Пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }*/
    }

}