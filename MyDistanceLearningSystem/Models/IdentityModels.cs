﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;

namespace MyDistanceLearningSystem.Models
{
    // Чтобы добавить данные профиля для пользователя, можно добавить дополнительные свойства в класс ApplicationUser. Дополнительные сведения см. по адресу: http://go.microsoft.com/fwlink/?LinkID=317594.
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [StringLength(15, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 15 символов")]
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Required]
        [StringLength(15, MinimumLength = 2, ErrorMessage = "Длина строки должна быть от 2 до 15 символов")]
        [Display(Name = "Фамилия")]
        public string Surname { get; set; }
        
        public int? GroupId { get; set; }
        [Display(Name = "Группа")]
        public Group Group { get; set; }
        
        public int? DisciplineId { get; set; }
        [Display(Name = "Дисциплина")]
        public Discipline Discipline { get; set; }

        [Display(Name = "Файлы")]
        public virtual ICollection<ApplicationFile> ApplicationFiles { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Обратите внимание, что authenticationType должен совпадать с типом, определенным в CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Здесь добавьте утверждения пользователя
            return userIdentity;
        }
    }
    public class ApplicationRole : IdentityRole
    {
        public ApplicationRole() : base() { }

        public ApplicationRole(string name)
            : base(name)
        { }
    }

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<TrainingProgram> TrainingPrograms { get; set; }
        public DbSet<Discipline> Disciplines { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<ApplicationFile> ApplicationFiles { get; set; }
        public DbSet<Message> Messages { get; set; }
        //public DbSet<HomeWork> HomeWorks { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            Database.SetInitializer<ApplicationDbContext>(null);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        //public System.Data.Entity.DbSet<MyDistanceLearningSystem.Models.ApplicationUser> ApplicationUsers { get; set; }

        /* public System.Data.Entity.DbSet<MyDistanceLearningSystem.Models.ApplicationUser> ApplicationUsers { get; set; }*/
    }
}