﻿function modal_window(obj, url) {
    $.ajaxSetup({ cache: false });
    $("."+ obj).click(function (e) {
        setTimeout("$('.spinner').toggle()", 1000);
        e.preventDefault();
        //$("."+ obj).attr("id")
        $.get(url, { id: $(this).attr("id") }, function (data) {
            $(".spinner").toggle();
            $('#dialogContent').html(data);
            $('#modDialog').modal('show');
        });
    });
}
function modal_window_with_param(obj, url, param) {
    $.ajaxSetup({ cache: false });
    $("." + obj).click(function (e) {
        setTimeout("$('.spinner').toggle()", 1000);
        e.preventDefault();
        //$("."+ obj).attr("id")
        $.get(url, param, function (data) {
            $(".spinner").toggle();
            $('#dialogContent').html(data);
            $('#modDialog').modal('show');
        });
    });
}

