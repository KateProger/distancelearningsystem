﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MyDistanceLearningSystem.Startup))]
namespace MyDistanceLearningSystem
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
